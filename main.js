const {app} = require('electron')
const createMainWindow = require('./mainWindow')
require('./ipcMainHandler')

app.whenReady().then(createMainWindow);

