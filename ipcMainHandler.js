const {ipcMain} = require('electron')
const {getUserDirectories, getUserDirectoriesByPath, openFile, createDir, createFile, deleteFile, deleteDirectory, renameFile} = require('./sevice/fileHandler')
ipcMain.on('submit:string', (event, args) => {
  console.log(args, event);
})

ipcMain.on('get:userDirectories', (event, directory) => {
  getUserDirectories(event, directory);
})

ipcMain.on('get:userDirectoriesByPath', (event, path) => {
  getUserDirectoriesByPath(event, path);
})

ipcMain.on('openFile', (event, filePath) => {
  openFile(event, filePath)
})

ipcMain.on('createDir', (event, filePath) => {
  createDir(event, filePath)
})

ipcMain.on('createFile', (event, filePath) => {
  createFile(event, filePath)
})

ipcMain.on('deleteFile', (event, filePath) => {
  deleteFile(event, filePath)
})

ipcMain.on('deleteDirectory', (event, filePath) => {
  deleteDirectory(event, filePath)
})

ipcMain.on('renameFile', (event, filePath) => {
  renameFile(event, filePath)
})
