const {app, ipcMain, shell } = require('electron')
const fs = require('fs');
const path = require('path');

function getUserDirectories(event, directory) {
  const userHomeDir = app.getPath(directory);
  fs.readdir(userHomeDir, {withFileTypes: true}, (err, files) => {
    if (err) {
      event.reply('userDirectories:error', err.message)
      return
    }
    const directories = getArrayFiles(files);
    event.sender.send('userDirectories:result', {directories: directories});
  })
}

function getUserDirectoriesByPath(event, newPath) {
  fs.readdir(newPath, {withFileTypes: true}, (err, files) => {
    if (err) {
      event.reply('userDirectoriesByPath:error', err.message)
      return
    }
    const directories = getArrayFiles(files);
    event.sender.send('userDirectoriesByPath:result', {directories: directories});
  })
}

function getArrayFiles(files)  {
  const arrFiles = files.map(file => {
    const fullPath = `${file.path}\\${file.name}`
    const stats = fs.statSync(fullPath);
    const extension = path.extname(fullPath)
    return {
      name: file.name,
      path: fullPath,
      isDir: file.isDirectory(),
      time: stats.birthtime,
      extension: file.isDirectory() ? 'Папка с файлами' : extension,
      size: stats.size > 0 ? (stats.size / 1024 / 1024).toFixed(2) : null,
  }
  })
  return arrFiles;
}

function openFile(event, filePath) {
  shell.openPath(filePath).then(() => {
    event.sender.send('openFile:opened');
  }).catch(err => {
    event.sender.send('openFile:error');
  });
}

function createDir(event, filePath) {
  fs.mkdir(filePath, {recursive: true}, (err) => {
    if (err) {
      event.sender.send('createDir:error', err.message)
      return
    }
    event.sender.send('createDir:success');
  })
}

function createFile(event, filePath) {
  fs.writeFile(filePath, '', (err) => {
    if (err) {
      event.sender.send('createFile:error', err.message)
      return
    }
    event.sender.send('createFile:success');
  })
}

function deleteFile(event, filePath) {
  fs.unlink(filePath,  (err) => {
    if (err) {
      event.sender.send('deleteFile:error', err.message)
      return
    }
    event.sender.send('deleteFile:success');
  })
}

function deleteDirectory(event, filePath) {
  fs.rmdir(filePath, {recursive: true},  (err) => {
    if (err) {
      event.sender.send('deleteDirectory:error', err.message)
      return
    }
    event.sender.send('deleteDirectory:success');
  })
}

function renameFile(event, filePath) {
  fs.rename(filePath[0], filePath[1],  (err) => {
    if (err) {
      event.sender.send('renameFile:error', err.message)
      return
    }
    event.sender.send('renameFile:success');
  })
}

module.exports = { getUserDirectories, getUserDirectoriesByPath, openFile, createDir, createFile, deleteFile, deleteDirectory, renameFile };

process.on('uncaughtException', (err) => {
  console.log('Ошибка', err);
})
