import { defineStore } from 'pinia';

export const useFileStore = defineStore({
  id: 'file',
  state: () => ({
    files: [],
    path: '',
    openModalCreated: false,
    isDir: false,
    filePath: '',
    isEditMode: false
  }),
  actions: {
    setFiles(files) {
      this.files = files;
    },
    setOpenModalCreated(value) {
      this.openModalCreated = value;
    },
    setPath(newPath) {
      this.path = newPath;
    },
    setIsDir(value) {
      this.isDir = value;
    },
    setFilePath(newFilePath) {
      this.filePath = newFilePath
    },
    setEditMode(value) {
      this.isEditMode = value;
    }
  }
})
