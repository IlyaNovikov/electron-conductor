const ipcRenderer = window.ipcRenderer;
export const getUserDirectories = (directory) => {
  return new Promise((resolve, reject) => {
    ipcRenderer.send('get:userDirectories', directory);

    ipcRenderer.on('userDirectories:result', (directories) => {
      resolve(directories);
    })

    ipcRenderer.on('userDirectories:error', (event, err) => {
      reject(err);
    })
  })
}

export const getUserDirectoriesByPath = (path) => {
  return new Promise((resolve, reject) => {
    ipcRenderer.send('get:userDirectoriesByPath', path);

    ipcRenderer.on('userDirectoriesByPath:result', (directories) => {
      resolve(directories);
    })

    ipcRenderer.on('userDirectoriesByPath:error', (event, err) => {
      reject(err);
    })
  })
}

export const openFile = (filePath) => {
  return new Promise((resolve, reject) => {
    ipcRenderer.send('openFile', filePath);

    ipcRenderer.on('openFile:opened', () => {
      resolve(true);
    })

    ipcRenderer.on('openFile:error', (event, err) => {
      reject(err);
    })
  })
}

export const createDir = (filePath) => {
  return new Promise((resolve, reject) => {
    ipcRenderer.send('createDir', filePath);

    ipcRenderer.on('createDir:success', () => {
      resolve(true);
    })

    ipcRenderer.on('createDir:error', (event, err) => {
      reject(err);
    })
  })
}

export const createFile = (filePath) => {
  return new Promise((resolve, reject) => {
    ipcRenderer.send('createFile', filePath);

    ipcRenderer.on('createFile:success', () => {
      resolve(true);
    })

    ipcRenderer.on('createFile:error', (event, err) => {
      reject(err);
    })
  })
}

export const deleteFile = (filePath) => {
  return new Promise((resolve, reject) => {
    ipcRenderer.send('deleteFile', filePath);

    ipcRenderer.on('deleteFile:success', () => {
      resolve(true);
    })

    ipcRenderer.on('deleteFile:error', (event, err) => {
      reject(err);
    })
  })
}

export const deleteDirectory = (filePath) => {
  return new Promise((resolve, reject) => {
    ipcRenderer.send('deleteDirectory', filePath);

    ipcRenderer.on('deleteDirectory:success', () => {
      resolve(true);
    })

    ipcRenderer.on('deleteDirectory:error', (event, err) => {
      reject(err);
    })
  })
}

export const renameFile = (filePath) => {
  return new Promise((resolve, reject) => {
    ipcRenderer.send('renameFile', filePath);

    ipcRenderer.on('renameFile:success', () => {
      resolve(true);
    })

    ipcRenderer.on('renameFile:error', (event, err) => {
      reject(err);
    })
  })
}
