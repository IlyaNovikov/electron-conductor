export const defaultLinks = [
  {
    name: 'Загрузки', dir: 'downloads', isDir: true, extension: 'Папка с файлами'
  },
  {
    name: 'Документы', dir: 'documents', isDir: true, extension: 'Папка с файлами'
  },
  {
    name: 'Рабочий стол', dir: 'desktop', isDir: true, extension: 'Папка с файлами'
  },
  {
    name: 'Музыка', dir: 'music', isDir: true, extension: 'Папка с файлами'
  },
  {
    name: 'Изображения', dir: 'pictures', isDir: true, extension: 'Папка с файлами'
  },
  {
    name: 'Видео', dir: 'videos', isDir: true, extension: 'Папка с файлами'
  },
  {
    name: 'User', dir: 'home', isDir: true, extension: 'Папка с файлами'
  },
];
