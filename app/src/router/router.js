import MainPage from '@/pages/MainPage.vue';
import { createMemoryHistory, createRouter } from 'vue-router';

const routes = [
  {
    path: '/', component: MainPage
  }
]

const router = createRouter({
  history: createMemoryHistory(),
  routes,
})

export default router;
